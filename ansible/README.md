# comp90023-assignment-2

## Ansible

Use Ansible to create new instances for database

```
./create-instances.sh
```

Use Ansible to create new instance and build web app

```
./build-web.sh
```

Use Ansible to create cluster

```
./create-cluster.sh
```