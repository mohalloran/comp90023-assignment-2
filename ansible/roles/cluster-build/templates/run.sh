#!/usr/bin/env bash
sudo service docker restart

echo "== Set variables =="
export user=admin
export pass=admin
export VERSION='3.0.0'
export cookie='a192aeb9904e6590849337933b000c99'
export uuid='a192aeb9904e6590849337933b001159'
export node="$1"
export flag="-setcookie \"${cookie}\" -name \"couchdb@${node}\""

echo "== Remove old containers =="
if [ ! -z $(sudo docker ps -a -q) ] 
  then
    sudo docker rm -f $(sudo docker ps -a -q)
fi

if [ ! -z $(sudo docker network ls) ] 
  then
    sudo docker network rm -f $(sudo docker network ls) 
fi

echo "== Pull image =="
sudo docker pull ibmcom/couchdb3:${VERSION}

echo "== Create Docker containers =="
if [ ! -z $(sudo docker ps --all --filter "name=couchdb${node}" --quiet) ] 
    then
      sudo docker stop $(sudo docker ps --all --filter "name=couchdb${node}" --quiet) 
      sudo docker rm $(sudo docker ps --all --filter "name=couchdb${node}" --quiet)
fi 

sudo docker create\
  -p 5984:5984\
  -p 4369:4369\
  -p 9100-9200:9100-9200\
  -p 5986:5986\
  --name couchdb${node}\
  --env COUCHDB_USER=${user}\
  --env COUCHDB_PASSWORD=${pass}\
  --env NODENAME=couchdb@${node}\
  --env COUCHDB_SECRET=${cookie}\
  --env ENABLE_CORS=true\
  --env ERL_FLAGS="-setcookie \"${cookie}\" -name \"couchdb@${node}\""\
  ibmcom/couchdb3:${VERSION}

echo "== Start the containers =="
sudo docker start couchdb${node}

echo "== Edit local.ini =="
sudo docker cp local.ini couchdb${node}:/local.ini
sudo docker exec -u root -it couchdb${node} /bin/sh -c "mv local.ini /opt/couchdb/etc/local.d;exit"

sudo docker restart couchdb${node}