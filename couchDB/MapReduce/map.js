function (doc) {
  var editDate = new Date(doc.created_at.split(" ")[0]);
  var month = editDate.getMonth()+1;
  var year = editDate.getFullYear();
  var date = editDate.getDate();

  if(year==2020){

    if(date<=10){
      stage='F'
    }
    else if(date<=20){
      stage='M'
    }
    else{
      stage='L'
    }
  if(doc.sentiment[0]<0)
    emit([doc.state,'negative',month,stage],1)
  else if(doc.sentiment[0]>0)
    emit([doc.state,'positive',month,stage],1)  
  else 
    emit([doc.state,'neutral',month,stage],1)
 
}}