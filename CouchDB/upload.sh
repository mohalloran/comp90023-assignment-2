export declare -a nodes=(172.26.132.221 172.26.130.249 172.26.133.239)
export user=admin
export pass=admin
export VERSION='3.0.0'
export cookie='a192aeb9904e6590849337933b000c99'
export uuid='a192aeb9904e6590849337933b001159'
export masternode=`echo ${nodes} | cut -f1 -d' '`
export declare -a othernodes=`echo ${nodes[@]} | sed s/${masternode}//`
export size=${#nodes[@]}

# curl -XPUT "http://${user}:${pass}@${masternode}:5984/twitter"
curl -XPUT "http://${user}:${pass}@${masternode}:5984/aurin-data"

# curl -XPOST "http://${user}:${pass}@${masternode}:5984/twitter/_bulk_docs " --header "Content-Type: application/json" \
#   --data @../twitter/data.json

curl -XPOST "http://${user}:${pass}@${masternode}:5984/aurin-data/_bulk_docs " --header "Content-Type: application/json" \
  --data @aurin-data.json
