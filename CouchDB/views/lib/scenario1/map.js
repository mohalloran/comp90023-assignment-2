function (doc) {
  if(doc.sentiment[0]<0)
    emit([doc.state,'negative'],1)
  else if(doc.sentiment[0]>0)
    emit([doc.state,'positive'],1)  
  else 
    emit([doc.state,'neutral'],1)
}