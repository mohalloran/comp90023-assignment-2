import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import ReactChartkick, { PieChart, ColumnChart } from 'react-chartkick';
import Chart from 'chart.js'
ReactChartkick.addAdapter(Chart)
class Info extends Component {
  constructor(props){
    super(props)
    this.state = {
      neg : null,
      pos:null,
      neu:null,
      info:null,
      total:null,
      city: null

    }
  }

  updateState = (that,props) => {
    that.setState({neg:parseFloat(props.pie[0].values[0]),
      neu:parseFloat(props.pie[0].values[1]),
      pos:parseFloat(props.pie[0].values[2]),
      total:parseFloat(props.pie[0].values[0] + props.pie[0].values[1] + props.pie[0].values[2]),
      info : props.info[0].values,
      city:props.city
      })
  }
    componentDidMount(){
      let that = this
      that.updateState(that,that.props)   
    }

    componentDidUpdate(prevProps) {
      let that = this
      if(prevProps.pie && this.props.pie && prevProps.pie !== this.props.pie && prevProps.pie[0].values !== this.props.pie[0].values) {
            that.updateState(that,that.props)   
          }
  }
    
  
    render() {
        let negPer = ((this.state.neg / this.state.total)).toFixed(2)
        let neuPer = ((this.state.neu / this.state.total)).toFixed(2)
        let posPer = ((this.state.pos / this.state.total)).toFixed(2)
        let city = this.state.city
        let pos = this.state.pos
        let neg = this.state.neg
        let neu = this.state.neu
        return (
            <div style={{overflow:'hidden',paddingLeft:10,paddingRight:10}}>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <Paper style={{padding:5,marginBottom:5}}>
                    <p style={{textAlign:'center',fontWeight:600,textTransform:"capitalize"}}>Proportion of Sentiment Analysis for {city}.</p> 
                    {negPer !== null || negPer !== undefined ?<PieChart donut={true} data={[["Negative", negPer], ["Neutral", neuPer],["Positive", posPer]]} legend={true}/>
        :""}
                </Paper>
              </Grid>
              <Grid item xs={6}>
                <Paper style={{padding:5,marginBottom:5}}>
                    <p style={{textAlign:'center',fontWeight:600,textTransform:"capitalize"}}>Column Chart displaying absolute number of tweets for {city}.</p>
                       <ColumnChart dataset={{ borderWidth: 0, width: 0, barThickness: 30,maxBarThickness:35,backgroundColor: ['#FF9900', '#3366CC' ,'#DC3912'], }} colors={["#b00"]} stacked={true} data={[["Positive Tweets", pos], ["Negative", neg], ["Neutral", neu]]} xtitle="Sentiment Data" ytitle="Number of Tweets"/>
                </Paper>
              </Grid>
            </Grid>
          </div>
        );
    }
}

export default Info;