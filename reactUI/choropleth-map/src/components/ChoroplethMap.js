/* eslint-disable no-undef */
import React, { Component } from 'react';
import Datamap from 'datamaps/dist/datamaps.world.min.js';
import d3 from 'd3';
import AustraliaJson from './Australia.topo.json';
class ChoroplethMap extends Component {
    constructor(props) {
        super(props);
        this.updateMap = this.updateMap.bind(this)
    }

    updateMap(this_new,props){
        if(props.data){
        let dataset = {};

        let onlyValues = props.data.map(function (obj) { return obj.values; });
        let minValue = Math.min.apply(null, onlyValues),
            maxValue = Math.max.apply(null, onlyValues);

        let paletteScale = d3.scale.linear()
            .domain([minValue, maxValue])
            .range(["#ff926e", "#ff4800"]);
        if (props.addPer){
            props.data.forEach(function (item) { 
                let iso = item.state,
                value1 =item.values + "%", 
                value = item.values;
                dataset[iso] = { numberOfThings: value1, fillColor: paletteScale(value) };
            });
        } 
        else {
            props.data.forEach(function (item) { 
                let iso = item.state,
                value = item.values;
                dataset[iso] = { numberOfThings: value, fillColor: paletteScale(value) };
            });
        }
            

        let map = new Datamap({
            element: document.getElementById('cloropleth_map'),
            scope: 'australia',
            height:570,
            width:"100%",
            done: (datamap) => {
                datamap.svg.selectAll('.datamaps-subunit').on('click', (geography) => {
                    if(geography.id !== "CT"){
                        this_new.props.parentCallback(geography.id,geography.properties.name)
                    }
                    else {
                        alert('Data is not available for state ' + geography.properties.name)
                    }
                })},
            geographyConfig: {
                popupOnHover: true,
                hideAntarctica: false,
                highlightOnHover: true,
                highlightFillColor: '#000287',
                borderColor: '#444',
                highlightBorderWidth: 1,
                borderWidth: 1,
                borderOpacity: 1,
                highlightBorderOpacity: 1,
                dataJson: AustraliaJson,
                popupTemplate: function (geo, data) {
                    if (!data) { return ['<div class="hoverinfo"><strong>No data available</strong></div>']; }
                    return ['<div class="hoverinfo">',
                        '<strong>', geo.properties.name, '</strong>',
                        '<br>Count: <strong>', data.numberOfThings, '</strong>',
                        '</div>'].join('');
                }
            },
            fills: {
                HIGH: '#012140f',
                LOW: '#c8e1fa',
                MEDIUM: 'orange',
                UNKNOWN: 'rgb(0,0,0)',
                defaultFill: '#eee'
            },
            data: dataset,
            setProjection: function (element) {
                var projection = d3.geo.mercator()
                    .center([132, -28 ]) 
                    .scale(700)
                    .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
                var path = d3.geo.path().projection(projection);
                return { path: path, projection: projection };
            }
        }
        // map.legend()
        );
    }
}
    
    componentDidUpdate(prevProps){
        let this_new = this;
        if(prevProps.data && this.props.data && prevProps.data !== this.props.data && prevProps.data[0].values !== this.props.data[0].values) {
            let element = document.getElementById('cloropleth_map');
            element.innerHTML = ""
            this_new.updateMap(this_new,this.props)
        }
    }

    componentDidMount() {
        var this_new = this;
        this_new.updateMap(this_new,this_new.props)
    }
    render() {
        return (
            <div id="cloropleth_map" style={{
                height: "50%",
                width: "100%",
                overflow:'hidden',
                backgroundColor: '#a2a3a9'
            }}></div>
        );
    }
}

export default ChoroplethMap;