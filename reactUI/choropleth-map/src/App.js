import React, { Component } from 'react';
import './App.css';
import ChoroplethMap from './components/ChoroplethMap';
import $ from 'jquery';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Info from './components/Info';

const styles = {
  backgroundColor: '#a2a3a9'
};

class App extends Component {
  
  constructor(props){
    super(props)
    this.state = {
      data_values : null,
      reset: "none",
      defaultSelection : "total_covid19_case",
      aurinData:null,
      tweets:null,
      chartData: null,
      city:null,
      info:null,
      addPer:false
    }
    this.info_ref = React.createRef();
    this.populateAurinData = this.populateAurinData.bind(this)
  }
  
  componentDidMount(){
    var that = this;
    $.ajax({
        dataType: "json",
        url: "http://172.26.132.221:5984/aurin-data/_design/test/_view/test",
        xhrFields: {
            withCredentials: false
        },
        headers: {
            'Authorization': 'Basic ' + btoa('admin:admin')
            },
            success: function (response) {
              if(response.rows.length > 0){
                that.setState({aurinData:response})
                  that.populateAurinData(that,response,that.state.defaultSelection)
              }
            },
            error: function (err) {
                console.log("Error: " + err.responseText);
            }
    });
  }

  populateAurinData (that,response,tag) {
    let map_data = [] 
    for(var i = 0;i < (response.rows).length;i++){
        if(response.rows[i].key === "VIC" ){
          map_data.push({state:"VI",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
        })}
        else if(response.rows[i].key === "QLD" ){
          map_data.push({state:"QL",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
          })}
        else if(response.rows[i].key === "TAS" ){
          map_data.push({state:"TS",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
        })
        }
        else if(response.rows[i].key === "NSW" ){
          map_data.push({state:"NS",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
        })
        }
        else if(response.rows[i].key === "SA" ){
          map_data.push({state:"SA",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
        })
        }
        else if(response.rows[i].key === "WA" ){
          map_data.push({state:"WA",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
        })
        }
        else if(response.rows[i].key === "NT" ){
          map_data.push({state:"NT",values: parseFloat(response.rows[i].value[tag]).toFixed(2)
        })
        }
    }
    if(tag === "employ_growth_total"){
      that.setState({data_values:map_data,reset:'none',addPer:true})
    }
    else {
      that.setState({data_values:map_data,reset:'none',addPer:false})

    }
  }
  
  handleChange = (event) => {
    let that = this
    that.populateAurinData(that,that.state.aurinData,event.target.value)
  }

  updateChart = (that,response,selectedProv,city) => {
    let d = null
    if(selectedProv === "QL"){
      d = "QLD"
    }
    else if(selectedProv === "VI"){
      d = "VIC"
    }
    else if(selectedProv === "NS"){
      d = "NSW"
    }
    else if(selectedProv === "TS"){
      d = "TAS"
    }
    else if(selectedProv === "NT"){
      d = "NA"
    }
    else if(selectedProv === "WA"){
      d = "WA"
    }
    else if(selectedProv === "SA"){
      d = "SA"
    }
    let a = []
    for(let i = 0; i < this.state.data_values.length;i++){
      if(this.state.data_values[i].state === selectedProv ){
          a.push({
            state:this.state.data_values[i].state,
            values:this.state.data_values[i].values
          })
      }
    }
    let group = response.map((item)=> item.key[0] ).filter((item, i, ar) => ar.indexOf(item) === i).map(item=>{
            let new_list = response.filter(itm => itm.key[0] === item).map(itm=>(itm.value))
            return {state:item,values:new_list}
        }) 
    let data = []    
    for(let i = 0; i < group.length;i++){
      if(group[i].state === d){
        data.push({
          state:group[i].state,
          values: group[i].values
        })
      }
    }
    that.setState({reset:'block',chartData:data,city:city,info:a}, () => {
      this.info_ref.current.focus()
      })
    
  }


  callbackFunction = (childData,city) => {
    var that = this;
    $.ajax({
      dataType: "json",
      url: "http://admin:admin@172.26.132.221:5984/twitter/_design/count/_view/count-month?reduce=true&group_level=2",
      xhrFields: {
          withCredentials: false
      },
      headers: {
          'Authorization': 'Basic ' + btoa('admin:admin')
          },
          success: function (response) {
            if(response.rows.length){
              that.updateChart(that,response.rows,childData,city)
            }
          },
          error: function (err) {
              alert('Something went wrong, reload the page again')
          }
      }); 
  }

  reset = () => {
    this.setState({reset:'none'}, ()=> {
      this.populateAurinData(this,this.state.aurinData,this.state.defaultSelection)
    });
  }

  render() {
    return (
      <div style={styles}>
        {/* {this.state.data_values === null ? '' :<p style={{textAlign:'center',fontWeight:600,textTransform:"capitalize",width:'100%',marginTop:0,fontSize:'15'}}>Analyzing tweets for various scenarios relalted to COVID-19 in Australia</p>} */}
      <div style={{
        height:"90%",
        width: "100%",
        overflow:'hidden'
      }}>
        
        {this.state.data_values === null ? '' :<ChoroplethMap data={this.state.data_values} parentCallback = {(childData,city)=>this.callbackFunction(childData,city)} addPer = {this.state.addPer}/>}
      </div>
      <div style={{height:"10%", width:"100%",overflow:'hidden',display: "flex", justifyContent: "center",
          alignItems: "center"}}> 
      <Button variant="contained" style={{marginBottom:10,marginRight:30,backgroundColor:'#f70000'}} onClick={this.reset}>
        Reset
      </Button>
      <FormControl style={{width:'auto',marginBottom:13}}>
      <InputLabel>Scenario</InputLabel>
      <Select
          native
          onChange={this.handleChange}
        >
          <option aria-label="Scenario" value="total_covid19_case">Total Covid Cases</option>
          <option value="employ_growth_total"> Growth in Employment</option>
          <option value="net_migration"> Net Migration</option>
          <option value="property_sale_avg"> Average Price of Property Sales</option>
        </Select>
        </FormControl>
      </div>
      {this.state.chartData === null && this.state.chartData === null ? '': <div style={{display:this.state.reset,outline: 'none'}} ref={this.info_ref} tabIndex="0" >
        <Info pie = {this.state.chartData} city = {this.state.city} info = {this.state.info}/>
      </div>}
      </div>
    );
  }
}

export default App;
