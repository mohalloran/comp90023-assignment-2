from tweepy import API
from tweepy import OAuthHandler
from tweepy import Cursor
from tweepy.streaming import StreamListener
from tweepy import Stream
from tweepy import RateLimitError
from dateutil.parser import parse as dateparser
from dateutil import tz
from textblob import TextBlob
from twitter_harvest.auth import TwitterCredentials
import twitter_harvest.util.config as config
import couchdb
import json
import logging
import time
import re


# Class for managing Twitter's API operations
class TwitterAPI():
    def __init__(self, cred_rank=0, search_user=None):
        cred_count = len(TwitterCredentials.cred)
        self.auth = OAuthHandler(TwitterCredentials.cred[cred_rank % cred_count]["CONSUMER_KEY"],
                                 TwitterCredentials.cred[cred_rank % cred_count]["CONSUMER_SECRET"])
        self.auth.set_access_token(TwitterCredentials.cred[cred_rank % cred_count]["ACCESS_TOKEN"],
                                   TwitterCredentials.cred[cred_rank % cred_count]["ACCESS_TOKEN_SECRET"])
        self.twitter_api = API(self.auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
        self.search_user = search_user

    def search_tweets(self, state, db_twitter, db_users, geocode, filter_list):
        filter_list = " OR ".join(filter_list)

        logging.info("Searching %s: ", state)
        count = 0
        for page in limit_handled(
                Cursor(self.twitter_api.search, q=filter_list, geocode=geocode, count=100, since_id=None).pages()):
            try:
                for tweet in page:
                    tweet = tweet._json
                    # convert the data into a simplified format for easy manipulation in CouchDB
                    tweet_datetime = dateparser(tweet['created_at']).replace(tzinfo=tz.tzlocal())
                    if 'retweeted_status' in tweet:
                        retweet = True
                    else:
                        retweet = False
                    # Prepare a compact version of the tweets
                    mini_tweet = {
                        '_id': tweet['id_str'],
                        'id': tweet['id_str'],
                        'created_at': str(tweet_datetime),
                        'text': tweet['text'],
                        'user': tweet['user'],
                        'state': state,
                        'geo': tweet['geo'],
                        'coordinates': tweet['coordinates'],
                        'place': tweet['place'],
                        'lang': tweet['lang'],
                        'sentiment': TextBlob(tweet['text']).sentiment, #conduct sentiment analysis
                        'retweet': retweet
                    }
                    juser = mini_tweet['user']
                    mini_user = {
                        '_id': juser['id_str'],
                        'id': juser['id_str'],
                        'screen_name': juser['screen_name'],
                        'location': juser['location'],
                        'description': juser['description'],
                        'followers_count': juser['followers_count'],
                        'friends_count': juser['friends_count'],
                        'state': state,
                        'searched': False
                    }
                    try:
                        db_twitter.save(mini_tweet)
                        db_users.save(mini_user)
                    except couchdb.http.ResourceConflict:
                        pass
            except Exception as e:
                pass
        logging.info("Searched tweets: {} , in {}".format(count, state))
        return True

    def retrieve_timeline_tweets(self, state, db_twitter, search_user=None, filter_list=[], since_id=None):
        if search_user is None:
            user = self.search_user
        else:
            user = search_user

        try:
            for page in limit_handled(Cursor(self.twitter_api.user_timeline,
                                             user_id=search_user, count=200, since_id=since_id).pages()):
                for tweet in page:
                    try:
                        # Limit the search timeline to specific date
                        if dateparser(tweet._json['created_at']).replace(tzinfo=tz.tzlocal()) \
                                < dateparser(config.from_date).replace(tzinfo=tz.tzlocal()):
                            return True

                        # check whether this tweet meet our search criteria or not
                        if isMatching(tweet._json['text'], filter_list):
                            tweet = tweet._json

                            # convert the data into a simplified format for easy manipulation in CouchDB
                            tweet_datetime = dateparser(tweet['created_at']).replace(tzinfo=tz.tzlocal())

                            if 'retweeted_status' in tweet:
                                retweet = True
                            else:
                                retweet = False
                            # Prepare a compact version of the tweets
                            mini_tweet = {
                                '_id': tweet['id_str'],
                                'id': tweet['id_str'],
                                'created_at': str(tweet_datetime),  # tweet['created_at']
                                'text': tweet['text'],
                                'user': tweet['user'],
                                'state': state,
                                'geo': tweet['geo'],
                                'coordinates': tweet['coordinates'],
                                'place': tweet['place'],
                                'lang': tweet['lang'],
                                'sentiment': TextBlob(tweet['text']).sentiment, #conduct sentiment analysis
                                'retweet': retweet
                            }
                            try:
                                db_twitter.save(mini_tweet)
                            except couchdb.http.ResourceConflict:
                                pass
                    except Exception:
                        pass
        except Exception as e:
            logging.info("Interruption in retrieve_timeline_tweets ")
            return True

        return True

    def retrieve_friends(self, num_of_friends):
        friends = []
        for friend in limit_handled(Cursor(self.search_user.friends, id=self.search_user).items(num_of_friends)):
            friend.append(friend)
        return friends

    def limit_status(self):
        return self.twitter_api.rate_limit_status()


class TwitterStream:

    def __init__(self, cred_rank=0):
        cred_count = len(TwitterCredentials.cred)
        self.auth = OAuthHandler(TwitterCredentials.cred[cred_rank % cred_count]["CONSUMER_KEY"],
                                 TwitterCredentials.cred[cred_rank % cred_count]["CONSUMER_SECRET"])
        self.auth.set_access_token(TwitterCredentials.cred[cred_rank % cred_count]["ACCESS_TOKEN"],
                                   TwitterCredentials.cred[cred_rank % cred_count]["ACCESS_TOKEN_SECRET"])

    def stream(self, filename, bbox, filter_list):
        self.filename = filename
        logging.info("Streamer %s: starting", filename)
        listener = TwitterListener(filename)
        stream = Stream(self.auth, listener)
        stream.filter(track=filter_list, locations=bbox)


class TwitterListener(StreamListener):

    def __init__(self, filename):
        self.filename = filename

    def on_data(self, data):
        try:
            with open(self.filename, 'a', encoding="utf-8") as target:
                try:
                    tweet = json.loads(data)
                    target.write(data)
                except:
                    logging.info("Error %s: thread", self.filename + " : " + data)
                    pass
            return True
        except BaseException as e:
            print("Error on_data %s" % str(e))
        return True

    def on_error(self, status):
        logging.info("Error %s: thread", self.filename + str(status))
        if status <= 420:
            return False

    def on_timeout(self):
        time.sleep(5 * 60)
        return

# regulating the tweets flow to meet the API limits
def limit_handled(cursor):
    while True:
        try:
            yield cursor.next()
        except RateLimitError:
            logging.info("RateLimitException !!")
            time.sleep(15 * 60)
        except StopIteration as e:
            return False

# Check if the text is matching the search query
def isMatching(text, matchList):
    try:
        p = re.compile(r'(?:{})'.format('|'.join(map(re.escape, matchList))))
        if p.search(text.lower()):
            return True
        else:
            return False
    except Exception as e:
        print(e.args[0])
        return False
