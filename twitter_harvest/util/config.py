
# https://www.mapdevelopers.com/draw-circle-tool.php
VIC_GeoLocation = "-37.804541,144.811588,221km"
NSW_GeoLocation = "-33.152083,150.870422,495km"
QLD_GeoLocation = "-20.176761,149.449390,987km"
SA_GeoLocation = "-33.910191,134.486057,585km"
WA_GeoLocation = "-26.036284,118.163063,1062km"
NA1_GeoLocation = "-14.337986,133.759122,512km"
NA2_GeoLocation = "-21.805241,133.517536,460km"
TAS_GeoLocation = "-41.611075,146.880160,235km"
ACT_GeoLocation = "-35.516222,149.056436,45km"

VIC_BBox = [142.729921 ,-39.017529 ,148.003359 ,-36.233694]
NSW_BBox = [141.068634 ,-34.690775 ,153.593105 ,-29.030032]
QLD_BBox = [138.159371 ,-28.800968 ,156.001168 ,-11.474203]
SA_BBox = [129.062692 ,-38.060715 ,140.971871 ,-26.031704]
NT_BBox = [128.711129 ,-25.952703 ,137.983590 ,-10.913785]
WA_BBox = [111.836129 ,-34.772836 ,129.018746 ,-13.832312]
AU_BBox = [110.338446 ,-43.005195 ,154.782046 ,-11.867894]
WORLD_BBox = [-158.417411 ,-50.190013, 163.347554 ,73.687492]

States = [
        {'state_name': 'VIC','geocode': VIC_GeoLocation},
        {'state_name': 'NSW', 'geocode': NSW_GeoLocation},
        {'state_name': 'QLD', 'geocode': QLD_GeoLocation},
        {'state_name': 'NA', 'geocode': NA1_GeoLocation},
        {'state_name': 'WA', 'geocode': WA_GeoLocation},
        {'state_name': 'SA', 'geocode': SA_GeoLocation},
        {'state_name': 'TAS', 'geocode': TAS_GeoLocation}
    ]

Keywords = ['covid19', 'coronavirus', 'covid-19']
from_date = '2019/9/9'

DB_Config = 'http://admin:admin@172.26.133.58:5984'
#DB_Config = 'http://admin:admin@45.113.234.209:5984'
#DB_Config = 'http://admin:admin@127.0.0.1:5984'
